<?php
session_start();
if(empty($_SESSION["email_pay"]) && empty($_SESSION["amount_pay"]))
  exit();
  
$email = $_SESSION['email_pay'];
$amount = $_SESSION['amount_pay'];
// $email = $_REQUEST['email'];
// $amount = $_REQUEST['amount'];
// Merchant key here as provided by Payu
$MERCHANT_KEY = "1gB7DA";

// Merchant Salt as provided by Payu
$SALT = "CL5TgfXx";

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://sandboxsecure.payu.in";
// $PAYU_BASE_URL = "https://secure.payu.in";

$action = '';

$posted = array();
if(!empty($_POST)) {
    // print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
  
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
      || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
  $hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';  
  foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
  <head>
     <!-- Your Basic Site Informations -->
    <title>ZBH</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="ZBH">

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700" rel="stylesheet" type="text/css">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../resources/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resources/assets/css/slick.css">
    <link rel="stylesheet" href="../resources/assets/css/slick-theme.css">
    <link rel="stylesheet" href="../resources/assets/css/jquery.fancybox.css">
    <link rel="stylesheet" href="../resources/assets/css/animate.css">

    <link rel="stylesheet" href="../resources/assets/css/style.css">

    <!-- Custom Colors -->
    <!--<link rel="stylesheet" href="css/colors/blue/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/green/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/pink/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/purple/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/yellow/color.css">-->

   <!--[if lt IE 8]-->
   <link rel="stylesheet" href="../resources/assets/css/ie-older.css">
    <!-- [endif] -->

    <noscript><link rel="stylesheet" href="../resources/assets/css/no-js.css">
    </noscript>

    <!-- Favicons -->
    <link rel="shortcut icon" href="../resources/assets/images/favicon.ico">
    <link rel="apple-touch-icon" href="../resources/assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../resources/assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../resources/assets/images/apple-touch-icon-114x114.png">
  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  </head>
  <body onload="submitPayuForm()" class="payu-form">
    <!-- #header -->
<header id="header" data-parallax="scroll" data-speed="0.2" data-natural-width="1920" data-natural-height="1080" data-image-src="../resources/assets/images/content/bg/1.jpg">

    <!-- .header-overlay -->
    <div class="header-overlay">

        <!-- #navigation -->
        <nav id="navigation" class="navbar scrollspy">

            <!-- .container -->
            <div class="container">

                <div class="navbar-brand">
                    <a href="{{url('/')}}"><img src="../resources/assets/images/logo.png" alt="ZBH-Logo"></a> <!-- site logo -->
                </div>
              </div>
            </nav>
          </div>
        </header>
      <div class="container">
        <h2>PayU Form</h2>
        <br/>
        <?php if($formError) { ?>
      
          <span style="color:red">Please fill all mandatory fields.</span>
          <br/>
          <br/>
        <?php } ?>
        <form class="form-horizontal" action="<?php echo $action; ?>" method="post" name="payuForm">
          <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
          <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
          <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
        
              <p><b>Mandatory Parameters</b></p>
           
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="amount">Amount: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="amount" readonly value="<?php echo $amount; ?>" /></div>
              </div>
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="firstname">First Name: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" /></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="email">Email: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="email" id="email" readonly value="<?php echo $email; ?>" /></div>
              </div>
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="phone">Phone: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" /></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label class="control-label col-sm-2" for="product-info">Product Info: </label>
                <div class="col-sm-9 col-sm-offset-1 inp"><textarea rows="4" cols="10" name="productinfo"><?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?></textarea></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                  <!--Success URI:-->
                <label class="control-label col-sm-4 lft" for="product-info"> </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="surl" type="hidden" value="http://zbhpro.com/payu/response.php" size="64" /></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                  <!--Failure URI:-->
                <label class="control-label col-sm-4 lft" for="product-info"> </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="furl" type="hidden" value="http://zbhpro.com/payu/response.php" size="64" /></div>
                <div class="col-sm-12"><input type="hidden" name="service_provider" value="payu_paisa" size="64" /></div>
              </div>
            </div>
            <p><b>Optional Parameters</b></p>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="lastname">Last Name: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="lastname" id="lastname" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" /></div>
              </div>
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="cancel-uri">Cancel URI: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="curl" value="" /></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="address1">Address1: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="address1" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" /></div>
              </div>
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="address2">Address2: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" /></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="city">City: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="city" value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" /></div>
              </div>
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="state">State: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="state" value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" /></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="country">Country: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="country" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" /></div>
              </div>
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="zipcode">Zipcode: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" /></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="country">UDF1: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="udf1" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>" /></div>
              </div>
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="firstname">UDF2: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="udf2" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>" /></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="country">UDF3: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="udf3" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>" /></div>
              </div>
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="firstname">UDF4: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="udf4" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>" /></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="country">UDF5: </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" /></div>
              </div>
              <div class="col-sm-6">
                <label class="control-label col-sm-4" for="firstname">PG:  </label>
                <div class="col-sm-6 col-sm-offset-2 inp"><input name="pg" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>" /></div>
              </div>
            </div>
            <div class="row">
                <label id="checkout"></label>
                <?php if(!$hash) { ?>
                <div class="col-xs-12 chkout">
                    <button type="submit" id="payu-submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                </div>
                <?php } ?>
            </div>
          </table>
        </form>
    </div>

    <!-- #footer -->
<footer id="footer">

    <!-- .container -->
    <div class="container">

        <p class="copyright-txt">&copy; <?php echo date("Y"); ?> Copyrights by <a href="http://www.zbhpro.com/" target="_blank">ZBH</a> - All rights reserved.</p>

        <div class="socials">
            <a href="#" title="Facebook" class="link-facebook" ><i class="fa fa-facebook"></i></a>
            <a href="#" title="Twitter" class="link-twitter"><i class="fa fa-twitter"></i></a>
            <a href="#" title="Google Plus" class="link-google-plus"><i class="fa fa-google-plus"></i></a>
        </div>

    </div>
    <!-- .container end -->

</footer>
  </body>
</html>