@extends('layouts.style')

@section('main') 

<!-- Start Main Part -->

        <main>
            <div class="main-part">

                <section class="breadcrumb-nav">
                    <div class="container">
                        <div class="breadcrumb-nav-inner">
                            <ul>
                                <li><a href="{{ route ('welcome') }}">Home</a></li>
                                <li class="active"><a href="#">Service</a></li>
                            </ul>
                            <label class="now">SERVICE</label>
                        </div>
                    </div>
                </section>

                <!-- Start Service List -->   

                <section class="default-section service-main">
                    <div class="container">
                       <div class="service-main-partition dp-animation odd wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms">
                           <div class="row">
                               <div class="col-md-6 col-sm-6 col-xs-12 service-left-column wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                    <div class="service-partition-left">
                                       <img src="{{ asset('public/images/service1.jpg') }}" alt="" class="animated">
                                    </div>
                               </div>
                               <div class="col-md-6 col-sm-6 col-xs-12 service-right-column wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms">
                                    <div class="service-partition-right">
                                        <h5>INGREDIENT SUPPLY</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                        <a href="#" class="button-default button-dark-red">LEARN MORE</a>
                                    </div>
                               </div>
                           </div>
                       </div>
                       <div class="service-main-partition dp-animation even wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms">
                           <div class="row">
                               <div class="col-md-6 col-sm-6 col-xs-12 service-left-column wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                    <div class="service-partition-left">
                                       <img src="{{ asset('public/images/service2.jpg') }}" alt="" class="animated">
                                    </div>
                               </div>
                               <div class="col-md-6 col-sm-6 col-xs-12 service-right-column wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms">
                                    <div class="service-partition-right">
                                        <h5>EVENT ORDER</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                        <a href="#" class="button-default button-dark-red">LEARN MORE</a>
                                    </div>
                               </div>
                           </div>
                       </div>
                       <div class="service-main-partition dp-animation odd wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms">
                           <div class="row">
                               <div class="col-md-6 col-sm-6 col-xs-12 service-left-column wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                    <div class="service-partition-left">
                                       <img src="{{ asset('public/images/service3.jpg') }}" alt="" class="animated">
                                    </div>
                               </div>
                               <div class="col-md-6 col-sm-6 col-xs-12 service-right-column wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms">
                                    <div class="service-partition-right">
                                        <h5>SATISFIED CUSTOMERS</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                        <a href="#" class="button-default button-dark-red">LEARN MORE</a>
                                    </div>
                               </div>
                           </div>
                       </div>
                       <div class="service-main-partition dp-animation even wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms">
                           <div class="row">
                               <div class="col-md-6 col-sm-6 col-xs-12 service-left-column wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                    <div class="service-partition-left">
                                       <img src="{{ asset('public/images/service4.jpg') }}" alt="" class="animated">
                                    </div>
                               </div>
                               <div class="col-md-6 col-sm-6 col-xs-12 service-right-column wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms">
                                    <div class="service-partition-right">
                                        <h5>SATISFIED EMPLOYEES</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                        <a href="#" class="button-default button-dark-red">LEARN MORE</a>
                                    </div>
                               </div>
                           </div>
                       </div>
                    </div>
                </section>

                <!-- End Service List -->

                <!-- Start Partner Blog -->
                
                <section class="default-section partner-main text-center pad-top-remove wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms">
                    <div class="container">
                        <div class="owl-carousel owl-theme" data-items="5" data-tablet="3" data-mobile="2" data-nav="true" data-dots="false" data-autoplay="true" data-speed="1500" data-autotime="1800">
                            <div class="item dp-animation">
                                <img src="{{ asset('public/images/partner1.png') }}" alt="">
                            </div>
                            <div class="item dp-animation">
                                <img src="{{ asset('public/images/partner2.png') }}" alt="">
                            </div>
                            <div class="item dp-animation">
                                <img src="{{ asset('public/images/partner3.png') }}" alt="">
                            </div>
                            <div class="item dp-animation">
                                <img src="{{ asset('public/images/partner4.png') }}" alt="">
                            </div>
                            <div class="item dp-animation">
                                <img src="{{ asset('public/images/partner5.png') }}" alt="">
                            </div>
                            <div class="item dp-animation">
                                <img src="{{ asset('public/images/partner1.png') }}" alt="">
                            </div>
                            <div class="item dp-animation">
                                <img src="{{ asset('public/images/partner2.png') }}" alt="">
                            </div>
                            <div class="item dp-animation">
                                <img src="{{ asset('public/images/partner3.png') }}" alt="">
                            </div>
                            <div class="item dp-animation">
                                <img src="{{ asset('public/images/partner4.png') }}" alt="">
                            </div>
                            <div class="item dp-animation">
                                <img src="{{ asset('public/images/partner5.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </section>

                <!-- End Partner Blog -->

            </div>
        </main>  

        <!-- End Main Part -->


@endsection