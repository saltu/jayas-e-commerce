@extends('layouts.style')

@section('main')


<!-- Start Main Part -->

<main>
    <div class="main-part">

        <section class="breadcrumb-nav">
            <div class="container">
                <div class="breadcrumb-nav-inner">
                    <ul>
                        <li><a href="{{ url ('/') }}">Home</a></li>
                        <li><a href="{{ route ('menu') }}">Menu</a></li>
                        <li class="active"><a href="#">Cart</a></li>
                    </ul>
                    <label class="now">CART</label>
                </div>
            </div>
        </section>

        <!-- Start Shop Cart -->

        <section class="default-section shop-cart bg-grey">
            <div class="container">
                <div class="checkout-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <ul class="checkout-bar">
                        <li class="active">Shopping Cart</li>
                        <li>Checkout</li>
                        <li>Order Complete</li>
                    </ul>
                </div>
                <div class="shop-cart-list wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <table class="shop-cart-table">
                        <thead>
                            <tr>
                                <th>PRODUCT</th>
                                <th>PRICE</th>
                                <th>QUANTITY</th>
                                <th>TOTAL</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php 
                                $total_price = 0;
                                $i = 0; 
                            ?>

                            @if($cart_datas)
                            @foreach($cart_datas as $details)

                            <?php 
                                $i++; 
                                $product=DB::table('products')->select('*')->where('id',$details->products_id)->get();
                            ?>
                                <tr>
                                    <td data-th="Product">
                                        <div class="row">
                                            <div class="col-sm-3 hidden-xs"><img src="{{ 'public/products/medium/'.$product[0]->image }}" width="100" height="100" class="img-responsive" /></div>
                                            <div class="col-sm-9">
                                                <h4 class="nomargin">{{ $product[0]->p_name }}</h4>
                                            </div>
                                        </div>
                                    </td>
                                    <td data-th="Price">{{ $product[0]->price }}</td>
                                    <td data-th="Quantity">
                                        <a class="cart_quantity_up" href="{{ url('/cart/update-quantity/'.$details->id.'/1') }}"> + </a>
                                        <input style="height: 50px !important; width: 50% !important; border-radius: 0px !important; padding: 0px !important; margin-bottom: 0px !important; text-align: center !important;" type="text" name="quantity" value="{{ $details->quantity }}" autocomplete="off" size="2">
                                        @if($details->quantity > 1)
                                            <a class="cart_quantity_down" href="{{ url('/cart/update-quantity/'.$details->id.'/-1') }}"> - </a>
                                        @endif
                                    </td>
                                    <td data-th="Subtotal" class="text-center">{{ $details->quantity * $product[0]->price }}</td>
                                    <td class="actions" data-th="">

                                        <button class="btn btn-danger btn-sm removefromcart" data-id="{{ $details->id }}">
                                        <i class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                                <?php $total_price += $details->quantity * $product[0]->price ?>


                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
                <div class="cart-total wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms">
                    <div class="cart-total-title">
                        <h5>CART TOTALS</h5>
                    </div>
                    <div class="product-cart-total">
                        <small>Total products : <?php echo $i; ?></small>
                        <span></span>
                    </div>
                    <div class="grand-total">
                        <h5>TOTAL <span>{{ $total_price }}</span></h5>
                    </div>
                    <div class="proceed-check">
                        <a href="{{ route('checkout') }}" class="btn-primary-gold btn-medium">PROCEED TO CHECKOUT</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- End Shop Cart -->

    </div>
</main>

<!-- End Main Part -->


@endsection

@section('scripts')
 
 
    <script type="text/javascript">
 
        $(".update-cart").click(function (e) {
           e.preventDefault();
 
           var ele = $(this);
 
            $.ajax({
               url: '{{ url('update-cart') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
        });
 
        $(".removefromcart").click(function (e) {
            e.preventDefault();
 
            var ele = $(this);
 
            if(confirm("Are you sure")) {
                $.ajax({
                    url: '{{ url('removefromcart') }}',
                    method: "delete",
                    data: { _token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        alert(response);
                        window.location.reload();
                    }
                });
            }
        });
 
    </script>
 
@endsection
