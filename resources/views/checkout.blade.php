@extends('layouts.style')

@section('main')


<!-- Start Main Part -->

<main>
    <div class="main-part">

        <section class="breadcrumb-nav">
            <div class="container">
                <div class="breadcrumb-nav-inner">
                    <ul>
                        <li><a href="{{ url ('/') }}">Home</a></li>
                        <li><a href="{{ route ('menu') }}">Menu</a></li>
                        <li class="active"><a href="#">Shop Checkout</a></li>
                    </ul>
                    <label class="now">SHOP CHECKOUT</label>
                </div>
            </div>
        </section>

        <!-- Start Shop Cart -->

        <section class="default-section shop-checkout bg-grey">
            <div class="container">
                <div class="checkout-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <ul class="checkout-bar">
                        <li class="done-proceed">Shopping Cart</li>
                        <li class="active">Checkout</li>
                        <li>Order Complete</li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="shop-checkout-left">
                            <form class="form-checkout" name="form" action="{{ route('submit.order') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <h5>Billing Details</h5>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="text" name="first_name" placeholder="First Name" required>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="text" name="last_name" placeholder="Last Name" required>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="company" placeholder="Company Name">
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="email" name="email" placeholder="Email" required>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="text" name="phone" placeholder="Phone" required>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <select class="select-dropbox" name="country">
                                            <option>Country</option>
                                            <option>India</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <textarea placeholder="Address" name="address" required></textarea>
                                    </div>

                                </div>

                        </div>
                    </div>
                    <?php
                    $total_price = 0;
                    $i = 0;
                    ?>
                    <div class="col-md-5 col-sm-5 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="shop-checkout-right">
                            <div class="shop-checkout-box">
                                <h5>YOUR ORDER</h5>
                                <div class="shop-checkout-title">
                                    <h6>PRODUCT <span>TOTAL</span></h6>
                                </div>
                                <div class="shop-checkout-row">
                                    @if($cart_datas)
                                    @foreach($cart_datas as $details)

                                    <?php
                                    $i++;
                                    $product = DB::table('products')->select('*')->where('id', $details->products_id)->get();
                                    ?>

                                    <p><span>{{ $product[0]->p_name }}</span> x {{ $details->quantity }} <small>{{ $product[0]->price * $details->quantity }}</small></p>

                                    {{ $total_price += $details->quantity * $product[0]->price }}

                                    @endforeach
                                    @endif
                                </div>
                                <div class="checkout-total">
                                    <h6>CART SUBTOTAL <small>{{ $total_price }}</small></h6>
                                </div>
                                <div class="checkout-total">
                                    <h6>SHIPPING <small>Free Shipping</small></h6>
                                </div>
                                <div class="checkout-total">
                                    <h6>ORDER TOTAL <small class="price-big">{{ $total_price }}</small></h6>
                                </div>
                            </div>
                            <div class="shop-checkout-box">
                                <h5>PAYMENT METHODS</h5>
                                <select class="form-control" name="payment_method" required>
                                    <option value="">--Select Method--</option>
                                    <option value="cod">Cash On Delivery</option>
                                    <option value="pay">Online</option>
                                </select>
                            </div>
                            <div class="checkout-button">
                                <button class="button-default btn-large btn-primary-gold" type="submit">PROCEED TO PAYMENT</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </section>

        <!-- End Shop Cart -->

    </div>
</main>

<!-- End Main Part -->




@endsection