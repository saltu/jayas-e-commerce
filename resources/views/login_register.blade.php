@extends('layouts.style')

@section('main')


<!-- Start Main Part -->

<main>
    <div class="main-part">

        <section class="breadcrumb-nav">
            <div class="container">
                <div class="breadcrumb-nav-inner">
                    <ul>
                        <li><a href="index_3997808.html">Home</a></li>
                        <li><a href="shop.html">Shop</a></li>
                        <li class="active"><a href="#">Login / Register</a></li>
                    </ul>
                    <label class="now">LOGIN REGISTER</label>
                </div>
            </div>
        </section>

        <!-- Start Login & Register -->

        <section class="default-section login-register bg-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="login-wrap form-common">
                            <div class="title text-center">
                                <h3 class="text-coffee">Login</h3>
                            </div>
                            <form class="login-form" action="{{url('/user_login')}}" method="post" name="login">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="email"  placeholder="Username or email address" class="input-fields" name="email"/>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="password" placeholder="********" class="input-fields" name="password"/>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label><input type="checkbox" name="chkbox">Remember me</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <a href="#" class="pull-right">Lost your password</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="submit" name="submit" value="LOGIN" class="button-default button-default-submit">
                                    </div>
                                </div>
                            </form>
                            <div class="divider-login">
                                <hr>
                                <span>Or</span>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <a href="#" class="facebook-btn btn-change button-default"><i class="fa fa-facebook"></i>Facebook Connect</a>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <a href="#" class="tweeter-btn btn-change button-default"><i class="fa fa-twitter"></i>Twitter Connect</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="register-wrap form-common">
                            <div class="title text-center">
                                <h3 class="text-coffee">Register</h3>
                            </div>
                            <form class="register-form" action="{{url('/register_user')}}" method="post" name="register">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                       <input type="text" placeholder="Name" name="name" value="{{old('name')}}"/>
                                       <span class="text-danger">{{$errors->first('name')}}</span>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                       <input type="email" placeholder="Email Address" name="email" value="{{old('email')}}"/>
                                       <span class="text-danger">{{$errors->first('email')}}</span>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="password" placeholder="Password" name="password" value="{{old('password')}}"/>
                                        <span class="text-danger">{{$errors->first('password')}}</span>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="password" placeholder="Confirm Password" name="password_confirmation" value="{{old('password_confirmation')}}"/>
                                        <span class="text-danger">{{$errors->first('password_confirmation')}}</span>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="submit" name="submit" class="button-default button-default-submit" value="RegIster now">
                                    </div>
                                </div>
                            </form>
                            <p>By clicking on “Register Now” button you are accepting the <a href="terms_condition.html">Terms &amp; Conditions</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- End Login & Register List -->

    </div>
</main>

<!-- End Main Part -->

@endsection
