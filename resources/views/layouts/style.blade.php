<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jaya's</title>

    <link href="https/fontsgoogleapiscom/css_7995475.css" rel="stylesheet">
    <link href="{{ asset('public/css/icon-plugin/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/icon-plugin/fontello.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/revolution-plugin/extralayers.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/revolution-plugin/settings.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/bootstrap-plugin/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/bootstrap-plugin/bootstrap-slider.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/animation/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/owl-carousel/owl.theme.default.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/light-box/simplelightbox.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/light-box/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/scroll-bar/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/form-field/jquery.formstyler.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/Slick-slider/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/uniform.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Page pre loader -->
    <!-- <div id="pre-loader">
        <div class="loader-holder">
            <div class="frame">
                <img src="{{ asset('public/images/Preloader.gif') }}" alt="Jayas'" />
            </div>
        </div>
    </div> -->
    <!-- Start Wrapper Part -->
    <div class="wrapper">
        <!-- Start Header Part -->
        <header>
            <div class="header-part header-transparent header-reduce sticky">
                <div class="header-nav">
                    <div class="screen-container">
                        <div class="header-nav-inside">
                            <div class="logo">
                                <a href="{{ url('/') }}"><span>Jaya's</span></a>
                            </div>
                            <div class="menu-top-login">
                                <div class="cell-part">
                                    <a href="{{ url('/') }}">Home</a>
                                </div>
                                <!-- <div class="cell-part">
                                    <a href="{{ route ('login_register') }}">Login</a>
                                </div> -->
                            </div>

                            <div class="menu-top-part">
                                 <a href="{{ route ('cart') }}">
                                 <div class="cart animated">
                                      <span class="icon-basket fontello"></span><span>Cart</span> 
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-category">
                    <div class="container">
                        <div class="category-icon">
                            <div class="category-icon-menu">
                                <a href="#" class="hambarger-icon">
                                    <span class="bar-1"></span>
                                    <span class="bar-2"></span>
                                    <span class="bar-3"></span>
                                </a>
                            </div>
                            <ul>
                                <li><a href="menu_fixed.html"><span class="cofee custom-icon"></span><strong>COFFEE</strong></a></li>
                                <li><a href="menu.html"><span class="milk custom-icon"></span><strong>MILK</strong></a></li>
                                <li><a href="menu_change.html"><span class="cocktail custom-icon"></span><strong>cocktail</strong></a></li>
                                <li><a href="menu_fixed.html"><span class="bewerages custom-icon"></span><strong>bewerages</strong></a></li>
                                <li><a href="menu.html"><span class="tea custom-icon"></span><strong>tea</strong></a></li>
                                <li><a href="menu_change.html"><span class="cake custom-icon"></span><strong>cake</strong></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- End Header Part -->


        @yield('main')


    <!-- Start Footer Part -->

    <footer>
        <div class="footer-part">
            <div class="footer-inner-info Banner-Bg" data-background="{{ asset('public/images/footer-bg.jpg') }}">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="logo">
                                <a href="index_3997808.html"><span>Despına</span><small>1991</small></a>
                            </div>
                            <p>We have a hankering for some really in good melt in a mouth variety. Floury is the best choice to taste food and dessert.</p>
                            <ul class="footer-social">
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <h5>Contact</h5>
                            <p>329 Queensberry Street, North Melbourne <br> VIC 3051, Australia. <br> <a href="tel/123 456 7890_983054.html">123 456 7890</a> <br> <a href="mailto:support@despina.com">support@despina.com</a></p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <h5>Opening Hour</h5>
                            <p>Monday - Friday: ........6am - 9pm <br> Saturday - Sunday: ........6am - 10pm <br> Close on special days</p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <h5> Despina Video</h5>
                            <a href="https://www.youtube.com/watch?v=uZ0aQMXkiV4" class="magnific-youtube"><img src="{{ asset('public/images/video-bg.png') }}" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <div class="container">
                    <p>Copyright © 2019 Jayas' Food Industry. All rights reserved.</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- End Footer Part -->


    </div>

    <!-- End Wrapper Part -->
<!-- Back To Top Arrow -->

    <a href="#" class="top-arrow"></a>

    <script src="{{ asset('public/js/jquery.min.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap/bootstrap-slider.js') }}"></script>
    <script src="{{ asset('public/js/revolution-plugin/jquery.themepunch.plugins.min.js') }}"></script>
    <script src="{{ asset('public/js/revolution-plugin/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('public/js/parallax-stellar/jquery.stellar.js') }}"></script>
    <script src="{{ asset('public/js/animation/wow.min.js') }}"></script>
    <script src="{{ asset('public/js/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('public/js/light-box/simple-lightbox.min.js') }}"></script>
    <script src="{{ asset('public/js/light-box/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('public/js/scroll-bar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('public/js/masonry/isotop.js') }}"></script>
    <script src="{{ asset('public/js/masonry/packery-mode.pkgd.min.js') }}"></script>
    <script src="{{ asset('public/js/form-field/jquery.formstyler.min.js') }}"></script>
    <script src="{{ asset('public/js/Slick-slider/slick.min.js') }}"></script>
    <script src="{{ asset('public/js/progress-circle/waterbubble.min.js') }}"></script>
    <script src="{{ asset('public/js/app2.js') }}"></script>
    <script src="{{ asset('public/js/script.js') }}"></script>
    @yield('scripts')
</body>

</html>