
@extends('layouts.style')

@section('main')



        <!-- Start Main Part -->

        <main>
            <div class="main-part">

                <section class="breadcrumb-nav">
                    <div class="container">
                        <div class="breadcrumb-nav-inner">
                            <ul>
                                <li><a href="index_3997808.html">Home</a></li>
                                <li class="active"><a href="#">Contact</a></li>
                            </ul>
                            <label class="now">Contact</label>
                        </div>
                    </div>
                </section>
                
                <!-- Start Contact Part -->

                <section class="default-section contact-part pad-top-remove">
                    <div class="map-outer">
                        <div id="map"></div>
                    </div>
                    <div class="container">
                        <div class="title text-center">
                            <h2 class="text-coffee">Contact Us</h2>
                            <h6>We are a second-generation family business established in 1972</h6>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <div class="contact-blog-row">
                                    <div class="contact-icon">
                                        <img src="{{ asset('public/images/location.png') }}" alt="">
                                    </div>
                                    <p>329 Queensberry Street, North Melbourne VIC 3051, Australia.</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms">
                                <div class="contact-blog-row">
                                    <div class="contact-icon">
                                        <img src="{{ asset('public/images/cell.png') }}" alt="">
                                    </div>
                                    <p><a href="tel/1234567890_3670073.html">123 456 7890</a></p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1100ms">
                                <div class="contact-blog-row">
                                    <div class="contact-icon">
                                        <img src="{{ asset('public/images/mail.png') }}" alt="">
                                    </div>
                                    <p><a href="mailto:support@despina.com">support@despina.com</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <h5 class="text-coffee">Leave us a Message</h5>
                                <p>Aenean massa diam, viverra vitae luctus sed, gravida eget est. Etiam nec ipsum porttitor, consequat libero eu, dignissim eros. Nulla auctor lacinia enim id mollis.</p>
                                <form class="form" method="post" name="contact-form">
                                    <div class="row">
                                        <div class="alert-container"></div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label>First Name *</label>
                                            <input name="first_name" type="text" required>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label>Last Name *</label>
                                            <input name="last_name" type="text" required>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label>Email *</label>
                                            <input name="email" type="email" required>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label>Subject *</label>
                                            <input name="subject" type="text" required>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label>Your Message *</label>
                                            <textarea name="message" required></textarea>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input name="submit" value="SEND MESSAGE" class="submit-btn send_message" type="submit">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <h5 class="text-coffee">Opening Hours</h5>
                                <ul class="time-list">
                                    <li><span class="week-name">Monday</span> <span>12-6 PM</span></li>
                                    <li><span class="week-name">Tuesday</span> <span>12-6 PM</span></li>
                                    <li><span class="week-name">Wednesday</span> <span>12-6 PM</span></li>
                                    <li><span class="week-name">Thursday</span> <span>12-6 PM</span></li>
                                    <li><span class="week-name">Friday</span> <span>12-6 PM</span></li>
                                    <li><span class="week-name">Saturday</span> <span>12-6 PM</span></li>
                                    <li><span class="week-name">Sunday</span> <span>Closed</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- End Contact Part -->

            </div>
        </main>  

        <!-- End Main Part -->

        @endsection