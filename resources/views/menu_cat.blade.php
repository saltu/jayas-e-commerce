@extends('layouts.style')

@section('main')

<!-- Start Main Part -->

<main>
    <div class="main-part">

        <section class="breadcrumb-nav">
            <div class="container">
                <div class="breadcrumb-nav-inner">
                    <ul>
                        <li><a href="{{ url ('/') }}">Home</a></li>
                        <li class="active"><a href="#">Shop</a></li>
                    </ul>
                    <label class="now">{{ $byCate->name }}</label>
                </div>
            </div>
        </section>
        @if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif

        <!-- Start Blog List -->

        <section class="default-section shop-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="blog-left-section">
                            <div class="blog-left-categories blog-common-wide wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <h5>Categories</h5>
                                <ul class="list">                                    
                                    @if (count($category) > 0)
                                    @foreach ($category as $cat)
                                        <li><a href="{{ url('cat',$cat->id) }}">{{ $cat->name }}</a></li>
                                    @endforeach
                                    @endif 
                                </ul>
                            </div>
                            <!-- <div class="blog-left-deal blog-common-wide wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <h5>Best Deals</h5>
                                <div class="best-deal-blog">
                                    <div class="best-deal-left">
                                        <img src="{{ asset('public/images/img20.png') }}" alt="">
                                    </div>
                                    <div class="best-deal-right">
                                        <p>Lasal Cheese</p>
                                        <p><strong>$ 15</strong></p>
                                    </div>
                                </div>
                                <div class="best-deal-blog">
                                    <div class="best-deal-left">
                                        <img src="{{ asset('public/images/img21.png') }}" alt="">
                                    </div>
                                    <div class="best-deal-right">
                                        <p>Lasal Cheese</p>
                                        <p><strong>$ 15</strong></p>
                                    </div>
                                </div>
                                <div class="best-deal-blog">
                                    <div class="best-deal-left">
                                        <img src="{{ asset('public/images/img22.png') }}" alt="">
                                    </div>
                                    <div class="best-deal-right">
                                        <p>Lasal Cheese</p>
                                        <p><strong>$ 15</strong></p>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8 col-xs-12">
                        <div class="blog-right-section">
                            <div class="row">
                                <?php $i = 1; ?>
                                @if (count($list_product)>0)
                                @foreach ($list_product as $item)
                                <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                    <div class="shop-main-list">
                                        <div class="shop-product">
                                            <img src="{{url('public/products/small',$item->image)}}" alt="">
                                            <div class="cart-overlay-wrap">
                                                <div class="cart-overlay">
                                                    <a href="{{ url('/addToCart/'.$item->id) }}" class="shop-cart-btn">ADD TO CART</a>
                                                </div>
                                            </div>
                                        </div>
                                        <h5><a href="{{url('/product-detail',$item->id) }}"> {{ $item->p_name }}</h5> </a>
                                        <h6>{{ $item->price }}</h6>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                                
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- End Blog List -->

    </div>
</main>

<!-- End Main Part -->
@endsection