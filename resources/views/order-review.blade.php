@extends('layouts.style')

@section('main')

<!-- Start Main Part -->

<main>
            <div class="main-part">

                <section class="breadcrumb-nav">
                    <div class="container">
                        <div class="breadcrumb-nav-inner">
                            <ul>
                                <li><a href="{{ url ('/') }}">Home</a></li>
                                <li><a href="{{ route ('checkout') }}">Menu</a></li>
                                <li class="active"><a href="#">Order Complate</a></li>
                            </ul>
                            <label class="now">ORDER COMPLATE</label>
                        </div>
                    </div>
                </section>

                <!-- Start Shop Cart -->   

                <section class="default-section shop-cart bg-grey">
                    <div class="container">
                        <div class="checkout-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <ul class="checkout-bar">
                                <li class="done-proceed">Shopping Cart</li>
                                <li class="done-proceed">Checkout</li>
                                <li class="active done-proceed">Order Complete</li>
                            </ul>
                        </div>
                        <div class="order-complete-box wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <img src="{{ asset('public/images/complete-sign.png') }}" alt="">
                            <p>Thank you for Shopping with <h4 style="color:red;">Jaya's</h4>. You will receive a confirmation email shortly.</p>
                            
                        </div>
                    </div>
                </section>

                <!-- End Shop Cart -->

            </div>
        </main>  

        <!-- End Main Part -->

        @endsection