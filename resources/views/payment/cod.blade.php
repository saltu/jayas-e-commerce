<meta http-equiv="refresh" content="5;url={{ route('order-review')}}">

@extends('layouts.style')
@section('main')


<section class="breadcrumb-nav">
    <div class="container">
        <div class="breadcrumb-nav-inner">
            <ul>
                <li><a href="{{ url ('/') }}">Home</a></li>
                <li><a href="{{ route ('menu') }}">Menu</a></li>
            </ul>
            <label class="now">Cash On Delivery</label>
        </div>
    </div>
</section>
<br>
<div class="container">
    <h3 class="text-center">YOUR ORDER HAS BEEN PLACED</h3>
    <p class="text-center">Thanks for your Order that use Options on Cash On Delivery</p>
    <p class="text-center">We will contact you by Your Email " {{ $user_order->email }} " or Your Phone Number " {{ $user_order->phone }} " .</p>
</div>
<div style="margin-bottom: 20px;"></div>
@endsection