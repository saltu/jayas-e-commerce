@extends('layouts.style')
@section('title','Review Order Page')

@section('main')
    

    <form action="https://www.saltu.in" method="POST">
    <script
        src="https://checkout.razorpay.com/v1/checkout.js"
        data-key="<?php echo $api_key;?>"
        data-amount="50"
        data-currency="INR"
        data-order_id="<?php echo $id;?>"
        data-buttontext="Pay with Razorpay"
        data-name="Acme Corp"
        data-description="A Wild Sheep Chase is the third novel by Japanese author Haruki Murakami"
        data-image="https://example.com/your_logo.jpg"
        data-prefill.name="Gaurav Kumar"
        data-prefill.email="gaurav.kumar@example.com"
        data-prefill.contact="9999999999"
        data-theme.color="#F37254"
    ></script>
    <input type="hidden" custom="Hidden Element" name="hidden">
    </form>
@endsection