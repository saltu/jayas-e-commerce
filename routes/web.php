<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/welcome', 'IndexController@welcome')->name('welcome');

// Route::get('/menu', 'IndexController@menu')->name('menu');

// Route::get('/product-detail/{id}','ProductsController@detialpro');

// Route::get('/cart', 'IndexController@cart')->name('cart');

// Route::get('/checkout', 'IndexController@checkout')->name('checkout');

// Route::get('/services', 'IndexController@services')->name('services');

// Route::get('/Terms_Conditions', 'IndexController@Terms_Conditions')->name('Terms_Conditions');

// Route::get('/faq', 'IndexController@faq')->name('faq');

// Route::get('/contact', 'IndexController@contact')->name('contact');

Route::get('/login_register', 'IndexController@login_register')->name('login_register');

Route::get('/about_us', 'IndexController@about_us')->name('about_us');

// Route::get('add-to-cart/{id}', 'ProductsController@addToCart');

// Route::patch('update-cart', 'ProductsController@update');
 

/* Jayas Ecommerce Location */
Route::get('/','IndexController@index');
Route::get('/list-products','IndexController@shop')->name('menu');
Route::get('/cat/{id}','IndexController@listByCat')->name('cats');
Route::get('/product-detail/{id}','IndexController@detialpro');
////// get Attribute ////////////
Route::get('/get-product-attr','IndexController@getAttrs');
///// Cart Area /////////
Route::get('/addToCart/{id}','CartController@addToCart');
Route::get('/viewcart','CartController@index')->name('cart');
Route::get('/cart/deleteItem/{id}','CartController@deleteItem');
Route::delete('removefromcart','ProductsController@remove')->name('removefromcart');
Route::get('/cart/update-quantity/{id}/{quantity}','CartController@updateQuantity');
/////////////////////////
/// Apply Coupon Code
Route::post('/apply-coupon','CouponController@applycoupon');
/// Simple User Login /////
Route::get('/login_page','UsersController@index')->name('login_register');
Route::post('/register_user','UsersController@register');
Route::post('/user_login','UsersController@login');
Route::get('/logout','UsersController@logout');

Route::get('/check-out','CheckOutController@index')->name('checkout');
Route::post('/submit-checkout','CheckOutController@submitcheckout');

Route::get('/order-review','OrdersController@index')->name('order-review');
Route::post('/submit-order','OrdersController@order')->name('submit.order');
// Route::get('/cod','OrdersController@cod');
Route::get('/cod/{email}/{phone}','OrdersController@cod')->name('cod');
Route::get('/paypal','OrdersController@paypal');


////// User Authentications ///////////
Route::group(['middleware'=>'User_middleware'],function (){
    Route::get('/myaccount','UsersController@account');
    Route::put('/update-profile/{id}','UsersController@updateprofile');
    Route::put('/update-password/{id}','UsersController@updatepassword');

    // Route::get('/check-out','CheckOutController@index')->name('checkout');
    // Route::post('/submit-checkout','CheckOutController@submitcheckout');
    
    // Route::get('/order-review','OrdersController@index');
    // Route::post('/submit-order','OrdersController@order');
    // Route::get('/cod','OrdersController@cod');
    // Route::get('/paypal','OrdersController@paypal');
});
///


/* Admin Location */
Auth::routes(['register'=>false]);
// Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix'=>'admin','middleware'=>['auth','admin']],function (){
    Route::get('/', 'AdminController@index')->name('admin_home');
    /// Setting Area
    Route::get('/settings', 'AdminController@settings');
    Route::get('/check-pwd','AdminController@chkPassword');
    Route::post('/update-pwd','AdminController@updatAdminPwd');
    /// Category Area
    Route::resource('/category','CategoryController');
    Route::get('delete-category/{id}','CategoryController@destroy');
    Route::get('/check_category_name','CategoryController@checkCateName');
    /// Products Area
    Route::resource('/product','ProductsController');
    Route::get('delete-product/{id}','ProductsController@destroy');
    Route::get('delete-image/{id}','ProductsController@deleteImage');
    /// Product Attribute
    Route::resource('/product_attr','ProductAtrrController');
    Route::get('delete-attribute/{id}','ProductAtrrController@deleteAttr');
    /// Product Images Gallery
    Route::resource('/image-gallery','ImagesController');
    Route::get('delete-imageGallery/{id}','ImagesController@destroy');
    /// ///////// Coupons Area //////////
    Route::resource('/coupon','CouponController');
    Route::get('delete-coupon/{id}','CouponController@destroy');
///
});