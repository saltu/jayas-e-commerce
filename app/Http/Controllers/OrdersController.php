<?php

namespace App\Http\Controllers;

use App\Model\Cart_model;
use App\Model\Orders_model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\This;
use Razorpay\Api\Api;

class OrdersController extends Controller
{
    public function index(){
        $session_id=Session::get('session_id');
        $cart_datas=Cart_model::where('session_id',$session_id)->get();
        $total_price=0;
        foreach ($cart_datas as $cart_data){
            $total_price+=$cart_data->price*$cart_data->quantity;
        }
        $address=DB::table('delivery_address')->where('users_id',Auth::id())->first();
        return view('order-review',compact('address','cart_datas','total_price'));
    }
    public function order(Request $request){
        $this->validate(request(),[
            'payment_method' => 'required',
        ]);
        $input_data=$request->all();
        $payment_method = $request['payment_method'];
        Orders_model::create($input_data);
        $request->session()->flush();
        if($payment_method == "cod"){
            return redirect()->route('cod', ['email' => $request->email, 'phone' => $request->phone]);
        }else{
            return redirect('/paypal');
        }

    }
    public function cod($email,$phone){
        $user_order=Orders_model::where('email',$email)->where('phone',$phone)->first();
        return view('payment.cod',compact('user_order'));
    }
    public function paypal(Request $request){
        // echo('gi');die();
        // $who_buying=Orders_model::where('users_id',Auth::id())->first();
        // return view('payment.paypal',compact('who_buying'));
        // $api_key = "rzp_test_05d4vicl3wDj32";
        // $api_secret = "WavyDwX4OBYTTqw3Df2fXxYk";
        // $api = new Api($api_key, $api_secret);
        // $order = $api->order->create(array(
        //   'receipt' => '123',
        //   'amount' => 5002500,
        //   'payment_capture' => 1,
        //   'currency' => 'INR'
        //   )
        // );
        // $id = $order->id;
        // return view('payment.paypal',compact('api_key', 'api_secret', 'id'));
        session_start();
        $_SESSION['email_pay'] = "nithinpvarghese12@gmail.com";
        $_SESSION['amount_pay'] = "1000";

        return redirect('payu/checkout.php');
    }
}
