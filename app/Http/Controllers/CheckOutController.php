<?php

namespace App\Http\Controllers;

use App\Model\User;
use App\Model\Orders_model;
use App\Model\Cart_model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session; 

class CheckOutController extends Controller
{
    public function index(){
       
        $user_login=User::where('id',Auth::id())->first();
        $session_id=Session::get('session_id');
        $cart_datas=Cart_model::where('session_id',$session_id)->get();
        return view('checkout',compact('user_login','cart_datas'));
    }
    public function submitcheckout(Request $request){
       $this->validate($request,[
           'first_name'=>'required',
           'last_name'=>'required',
           'company'=>'required',
           'email'=>'required',
           'phone'=>'required',
           'address'=>'required',
           'payment_method'=>'required',
           
       ]);
       
        $input_data=$request->all();
        DB::table('delivery_address')->insert([
            'email'=>$input_data['email'],
            'name'=>$input_data['first_name'],
            'address'=>$input_data['address'],
            'mobile'=>$input_data['phone'],
            
            ]);
            

   return redirect('/order-review');

}

}
