<?php

namespace App\Http\Controllers;

use App\Model\Cart_model;
use App\Model\ProductAtrr_model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function index(){
        $session_id=Session::get('session_id');
        $cart_datas=Cart_model::where('session_id',$session_id)->get();
        return view('cart',compact('cart_datas'));
    }

    public function addToCart($id){

        Session::forget('discount_amount_price');
        Session::forget('coupon_code');
        // $stockAvailable=DB::table('product_att')->select('stock','sku')->where('products_id'=>$id)->first();
       
        if (Auth::check()){
        $inputToCart['products_id']=$id;
        $inputToCart['product_name']=0;
        $inputToCart['size']=0;
        $inputToCart['product_code']=0;
        $inputToCart['product_color']=0;
        $inputToCart['price']=0;
        $inputToCart['quantity']=1;
        $inputToCart['user_email']="support@saltu.in";

        $count_duplicateItems=Cart_model::where('products_id',$id)->count();
        }
        else{
        $session_id=Session::get('session_id');
        if(empty($session_id)){
            $session_id=str_random(40);
            Session::put('session_id',$session_id);
        }
        $inputToCart['session_id']=$session_id;

        $inputToCart['products_id']=$id;
        $inputToCart['product_name']=0;
        $inputToCart['size']=0;
        $inputToCart['product_code']=0;
        $inputToCart['product_color']=0;
        $inputToCart['price']=0;
        $inputToCart['quantity']=1;
        $inputToCart['user_email']="support@saltu.in";

        $count_duplicateItems=Cart_model::where('products_id',$id)->where('session_id',$session_id)->count();
    }

        if($count_duplicateItems > 0){
            return back()->with('message','This Item Added already');
        }else{
            Cart_model::create($inputToCart);
            return back()->with('message','Add To Cart Already');
        }
        
    }
    public function deleteItem($id=null){
        $delete_item=Cart_model::findOrFail($id);
        Session::forget('discount_amount_price');
        Session::forget('coupon_code');
        $delete_item->delete();
        return back()->with('message','Deleted Success!');
    }
    public function updateQuantity($id,$quantity){
        Session::forget('discount_amount_price');
        Session::forget('coupon_code');
        $sku_size=DB::table('cart')->select('product_code','size','quantity')->where('id',$id)->first();
        $stockAvailable=DB::table('product_att')->select('stock')->where(['sku'=>$sku_size->product_code,
            'size'=>$sku_size->size])->first();
        $updated_quantity=$sku_size->quantity + $quantity;
        // if($stockAvailable->stock>=$updated_quantity){
            DB::table('cart')->where('id',$id)->increment('quantity',$quantity);
            return back()->with('message','Update Quantity already');
        // }else{
            // return back()->with('message','Stock is not Available!');
        // }
    }
}
