<?php

namespace App\Http\Controllers;

use App\Model\Category_model;
use App\Model\ImageGallery_model;
use App\Model\ProductAtrr_model;
use App\Model\Products_model;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        $products=Products_model::all();
        return view('welcome',compact('products'));
    }
    public function shop(){
        $products=Products_model::all();
        $category=Category_model::all();
        $byCate="";
        return view('menu',compact('products','category', 'byCate'));
    }
    public function listByCat($id){
        $list_product=Products_model::where('categories_id',$id)->get();
        $byCate=Category_model::select('name')->where('id',$id)->first();        
        $category=Category_model::all();
        return view('menu_cat',compact('list_product','category','byCate'));
    }
    public function detialpro($id){
        $detail_product=Products_model::findOrFail($id);
        $imagesGalleries=ImageGallery_model::where('products_id',$id)->get();
        $totalStock=ProductAtrr_model::where('products_id',$id)->sum('stock');
        $relateProducts=Products_model::where([['id','!=',$id],['categories_id',$detail_product->categories_id]])->get();
        return view('menu_single',compact('detail_product','imagesGalleries','totalStock','relateProducts'));
    }
    public function getAttrs(Request $request){
        $all_attrs=$request->all();
        //print_r($all_attrs);die();
        $attr=explode('-',$all_attrs['size']);
        //echo $attr[0].' <=> '. $attr[1];
        $result_select=ProductAtrr_model::where(['products_id'=>$attr[0],'size'=>$attr[1]])->first();
        echo $result_select->price."#".$result_select->stock;
    }

    public function login_register(){
        
        return view('login_register');
    }

    public function about_us(){
        return view('about_us');
    }
}
