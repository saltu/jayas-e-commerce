<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Orders_model extends Model
{
    protected $table='orders';
    protected $primaryKey='id';
    protected $fillable=['users_id',
        'first_name','last_name','address','email','phone',
        'payment_method','grand_total'];
}
